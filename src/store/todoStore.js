import {observable, action, computed} from 'mobx'; //mobx.js를 뜻함 이 mobx에서 사용할 함수들은 observable, action, computed이다.

class TodoStore {

    // static instance = new TodoStore();



    @observable
    _todos = [];

    @observable
    todo = {};

    @observable
    searchValue = '';

    @computed // 이게 호출이 됬을때 todos의 상태가 변경이됬다면 이걸 캐싱하고있다가 딱 호출되면 state가 안바뀌면 캐싱한걸 계속주다가 뭐가 바끼면 밑에걸 수행해서 줌.
    get todos(){
        return this._todos ? this._todos.slice() : [];
    }

    @action //observable 한 데이터를 건드릴땐 액션
    setTodoProp(name, value){
        this.todo = {
            ...this.todo, //spread연산이고 es6에 나옴(기존에값은 그대로 넣는거.(객체와 객체,배열과 배열 붙일때 많이씀.))
            [name] : value
        }
    }

    @action
    addTodo(todo){
        this.todos.push(todo);
    }

    @action
    clearTodo(){
        this.todo = {};
    }

    @action
    selectTodo(todo){
        this.todo=todo;
    }

    @action
    removeTodo(todoId){
        let index = this._todos.findIndex(todo => todo.id === todoId); // 자바의 람다 여기선 애로우 펑션ㄴ
        console.log('removeIndex: ' + index);
        if(index > -1){
            this._todos.splice(index, 1);
        }

        this.todo = {}
    }

    @action
    changeSearchValue(searchValue){
        this.searchValue = searchValue;
    }
}

export default new TodoStore();

