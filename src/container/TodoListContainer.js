import React, { Component } from 'react';

import {observer, inject} from 'mobx-react';

import TodoListView from '../view/TodoListView'

@inject('todoStore')
@observer
class TodoListContainer extends Component {

    onSelectTodo(todo){
        this.props.todoStore.selectTodo(todo);
    }
    render() {

        let { todos, searchValue } = this.props.todoStore;
        // const todos = this.props.todoStore.todos; 위와 같음
        todos = todos.filter(todo => todo.title.toLowerCase().indexOf(searchValue.toLowerCase()) !== -1);

        return (
            <TodoListView
                todos={todos}
                onSelectTodo={this.onSelectTodo.bind(this)}
            />

        );
    }
}

export default TodoListContainer;