import React, {Component} from 'react';

import {observer, inject} from 'mobx-react';

import {generateId} from '../IDGenerator'

import TodoEditFormView from '../view/TodoEditFormView';

@inject('todoStore')
@observer //store의 특정데이터를 지켜보겠다.
class TodoEditFormContainer extends Component {

    onSetTodoProp(name, value) {
        this.props.todoStore.setTodoProp(name, value);
    }

    onAddTodo() {
        alert('add!');
        let {todo} = this.props.todoStore;
        todo = {...todo, id: generateId(3)}
        this.props.todoStore.addTodo(todo);
        alert(todo.id);
    }

    onClearTodo() {
        this.props.todoStore.clearTodo();
    }

    onRemoveTodo() {
        const {todo} = this.props.todoStore;
        this.props.todoStore.removeTodo(todo.id);
    }

    /* ==
    import 해서 직접 '../todoStore' 해서 바로 todoStore.setTodoStore();할수있으나 리엑트스럽지않아 비권장
    */

    //event todo 값 둘다주자
    render() {

        const {todo} = this.props.todoStore;

        return (
            <TodoEditFormView
                todo={todo}
                onSetTodoProp={this.onSetTodoProp.bind(this)}
                onAddTodo={this.onAddTodo.bind(this)}
                onClearTodo={this.onClearTodo.bind(this)}
                onRemoveTodo={this.onRemoveTodo.bind(this)}
            />
        )
    }
}

export default TodoEditFormContainer;