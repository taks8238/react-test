import React, { Component } from 'react';

import { Input, Icon } from 'semantic-ui-react';

import { observer, inject } from 'mobx-react';

@inject('todoStore')
@observer
class SearchContainer extends Component {

    onChangeSearchValue(searchValue){
        this.props.todoStore.changeSearchValue(searchValue);
    }

    render(){
        return(
            <Input
            icon={<Icon name='search' inverted circular link />}
            placeholder='Search...'
            onChange={ (event) => this.onChangeSearchValue(event.target.value)} // arrow function 은 this바인딩 안해도됨
            />
        )
    }
}

export default SearchContainer;