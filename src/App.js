import React from 'react';
import { Container, Grid, Segment } from 'semantic-ui-react';
import TodoEditFormContainer from './container/TodoEditFormContainer';
import TodoListContainer from './container/TodoListContainer';
// import SearchContainer from './container/SearchContainer';

function App() {
  return (
    <Container >
      <Segment>
        {/* <SearchContainer /> */}
      </Segment>
      <Grid columns={2}>
        <Grid.Column>
          <Segment>
            <TodoEditFormContainer />
          </Segment>
        </Grid.Column>

        <Grid.Column>
          <Segment>
            <TodoListContainer />
          </Segment>
        </Grid.Column>
      </Grid>
    </Container>
  );
}

export default App;
