import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { Provider } from 'mobx-react';

import 'semantic-ui-css/semantic.min.css';
import './react-datepicker.css';
import todoStore from './store/todoStore';

ReactDOM.render(
    <Provider 
        todoStore={todoStore}
    >
        <App />
    </Provider>, 
    document.getElementById('root'));

serviceWorker.unregister();
